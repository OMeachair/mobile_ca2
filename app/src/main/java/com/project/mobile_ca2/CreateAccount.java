package com.project.mobile_ca2;

import com.project.mobile_ca2.db.entities.Account;

import android.support.v7.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.content.Context;
import android.widget.TextView;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.content.Intent;
import android.widget.Button;
import android.widget.Toast;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;

@SuppressWarnings("unused")
public class CreateAccount extends AppCompatActivity {

    private SharedPreferences loginRecords;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_account);

        Button accountBtn = findViewById(R.id.create_account_btn);

        accountBtn.setOnClickListener(view -> {

            Account account = getUserDetails();

            if(regexValidator(account)) {
                loginRecords = getSharedPreferences("login_details", Context.MODE_PRIVATE);

                if(checkRecords(account)) {
                    SharedPreferences.Editor editor = loginRecords.edit();
                    editor.putString(account.getUserName(), account.getPassword());
                    editor.apply();

                    Intent intent = new Intent(this, SearchPage.class);



                    Bundle bundle = new Bundle();
                    bundle.putString("username", account.getUserName());
                    intent.putExtras(bundle);

                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "NAME TAKEN", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Checks Shared preference if user name is already taken
     * @param account
     * @return
     */
    private boolean checkRecords(Account account) {
        Map<String, ?> recordsMap = loginRecords.getAll();
        Log.d("Records", recordsMap.toString());
        for(String userName : recordsMap.keySet()) {
            if(userName.equalsIgnoreCase(account.getUserName())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Extracts user details from TextView prompts and instantiates as new Account
     * @return
     */
    private Account getUserDetails() {
        TextView userNameHolder = findViewById(R.id.account_user_name);
        String userName = userNameHolder.getText().toString();

        TextView passwordHolder = findViewById(R.id.account_user_password);
        String password = passwordHolder.getText().toString();

        TextView emailHolder = findViewById(R.id.account_user_email);
        String email = emailHolder.getText().toString();

        return new Account(userName, email, password);
    }

    /**
     * Regex Strings used to test validity of new user details
     * @param account
     * @return
     */
    private boolean regexValidator(Account account) {
        final String NAME_REGEX = getApplicationContext().getResources().getString(R.string.regex_user_name);
        final String PASSWORD_REGEX = getApplicationContext().getResources().getString(R.string.regex_password);
        final String EMAIL_REGEX = getApplicationContext().getResources().getString(R.string.regex_email);

        return patternMatch(NAME_REGEX, account.getUserName())
                && patternMatch(EMAIL_REGEX, account.getEmail())
                && patternMatch(PASSWORD_REGEX, account.getPassword());
    }

    /**
     * Tests regex against user details
     * @param regex
     * @param value
     * @return
     */
    private boolean patternMatch(String regex, String value) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);

        return matcher.matches();
    }
}
