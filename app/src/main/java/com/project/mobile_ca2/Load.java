package com.project.mobile_ca2;

import android.app.ProgressDialog;

public class Load {
    private static int progressState = 0;

    /**
     * Displays a loading message
     * @param dialog
     * @param message
     */
    public static void loadingMessage(ProgressDialog dialog, String message) {
        dialog.setMessage(message);
        dialog.setCancelable(false);

        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setProgress(0);
        dialog.setMax(100);

        dialog.show();

        new Thread(() -> {
            while(progressState < 100) {
                progressState += 10;
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                dialog.setProgress(progressState);
            }

            if(progressState >= 100) dialog.dismiss();
        }).start();
    }
}
