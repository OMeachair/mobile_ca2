package com.project.mobile_ca2;

import android.support.v7.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.content.Context;
import android.widget.TextView;
import android.content.Intent;
import android.widget.Button;
import android.widget.Toast;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;

@SuppressWarnings("unused")
public class LoginAccount extends AppCompatActivity {

    private final String TAG = LoginAccount.class.getSimpleName();

    private SharedPreferences loginRecords;
    TextView txtUserName, txtPassword;
    String userName, promptPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_account);

        Button loginBtn = findViewById(R.id.login_button);

        loginBtn.setOnClickListener(view -> {
            loginRecords = getSharedPreferences("login_details", Context.MODE_PRIVATE);
            Log.d("Shared Preference", loginRecords.getAll().toString());

            if(validUserName()) {
                if(validPassword()) {
                    Intent intent = new Intent(this, SearchPage.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("username", userName);
                    intent.putExtras(bundle);

                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Invalid User Name", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Checks SharedPreference for existing user name
     * @return boolean
     */
    private boolean validUserName() {
        Map<String, ?> records = loginRecords.getAll();
        txtUserName = findViewById(R.id.login_name_prompt);
        userName = txtUserName.getText().toString();
        for (String name : records.keySet()) {
            if(userName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks SharedPreference for existing user name
     * @return
     */
    private boolean validPassword() {
        Map<String, ?> records = loginRecords.getAll();

        txtUserName = findViewById(R.id.login_name_prompt);
        userName = txtUserName.getText().toString();

        txtPassword = findViewById(R.id.login_password);
        promptPassword = txtPassword.getText().toString();

        return records.get(userName).equals(promptPassword);
    }
}
