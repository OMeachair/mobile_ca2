package com.project.mobile_ca2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import com.android.volley.toolbox.Volley;
import com.android.volley.RequestQueue;
import com.android.volley.Request;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getSimpleName();

    private SearchView searchBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();

        if(!sharedPref.contains("genres")){
            setGenres(editor);
        }

        Intent intent = new Intent(this,SignUpPage.class);
        startActivity(intent);

    }

    /**
     * Queries moviedb API to return Film genre names string to use with Genre ID's
     * @param editor
     */
    private void setGenres(final SharedPreferences.Editor editor){

        RequestQueue queue;

        queue = Volley.newRequestQueue(getApplicationContext());

        String key = getApplicationContext().getResources().getString(R.string.API_KEY);

        String url = "https://api.themoviedb.org/3/genre/movie/list?api_key="+key;

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    try {

                        Log.d(TAG, "onResponse: "+response);
                        editor.putString("genres",response).apply();

                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }, error -> Log.d(TAG,"GOT AN ERROR" + error));

        queue.add(stringRequest);
    }
}
