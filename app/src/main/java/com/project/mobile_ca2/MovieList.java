package com.project.mobile_ca2;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.project.mobile_ca2.adapters.MovieAdapter;

import android.support.constraint.ConstraintLayout;

import com.project.mobile_ca2.db.entities.Movie;
import com.project.mobile_ca2.utils.MovieParser;
import com.android.volley.toolbox.StringRequest;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.android.volley.toolbox.Volley;
import com.android.volley.RequestQueue;
import com.android.volley.Request;

import org.json.JSONObject;

import java.util.ArrayList;

import org.json.JSONArray;

import android.os.Bundle;
import android.view.View;
import android.util.Log;
import android.widget.Button;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MovieList extends AppCompatActivity {

    private final String TAG = MovieList.class.getSimpleName();

    MovieParser mParser = new MovieParser();

    List<Movie> movieList = new ArrayList<>();

    public ConstraintLayout singleMovieConstraint, movieListConstraint, posterFullscreenConstraint;
    RecyclerView recycler;

    private Button btnDelete;

    public boolean fullScreenFragment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_list);
        Load.loadingMessage(new ProgressDialog(this), "Retrieving Films");

        singleMovieConstraint = findViewById(R.id.singleMovieConstraint);
        movieListConstraint = findViewById(R.id.movieListConstraint);
        recycler = findViewById(R.id.recycler);

        Bundle bundle = getIntent().getExtras();

        String searchTermStr = null;
        int searchTermInt = 0;

        if (bundle != null) {
            searchTermStr = bundle.getString("searchTermStr");
            searchTermInt = bundle.getInt("searchTermInt");
        }

        if (searchTermStr != null) {
            readAPI(searchTermStr, 0);
        } else {
            searchTermStr = String.valueOf(searchTermInt);
            readAPI(searchTermStr, 1);

        }


    }

    /**
     * Passes List of Movie objects into adapter to populate RecyclerView
     * @param list
     */
    private void setAdapter(List<Movie> list) {
        MovieAdapter movieAdapter = new MovieAdapter(getBaseContext(), list);
        recycler.setAdapter(movieAdapter);
        recycler.setLayoutManager(new LinearLayoutManager(getApplication()));
    }

    /**
     * Queries moviedb API and instantiates volley thread
     * @param searchTerm
     * @param type
     */
    public void readAPI(String searchTerm, int type) {

        RequestQueue queue;

        queue = Volley.newRequestQueue(getApplicationContext());

        String key = getApplicationContext().getResources().getString(R.string.API_KEY);
        String searchType;
        String queryType;

        if (type == 0) {
            searchType = "search";
            queryType = "&query=";
        } else {
            searchType = "discover";
            queryType = "&year=";
        }

        String url = "https://api.themoviedb.org/3/" + searchType + "/movie?api_key=" + key + queryType + searchTerm;

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    JSONObject jsonObj;
                    try {
                        int count = 0;
                        jsonObj = new JSONObject(response);

                        JSONArray jsonArray = jsonObj.getJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            count++;
                            JSONObject jsonMovie = jsonArray.getJSONObject(i);
                            Movie movie = mParser.parseMovie(count, jsonMovie);

                            movieList.add(movie);
                        }
                        Collections.sort(movieList, getOrder());
                        setAdapter(movieList);
                        Log.d("gone", "threw");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }, error -> Log.d("API", "GOT AN ERROR" + error));

        queue.add(stringRequest);
    }

    /**
     * Based on current SortOrder enum value, returns the sorting pattern of API data
     * @return
     */
    private Comparator<Movie> getOrder() {
        SortOrder order = (SortOrder) getIntent().getSerializableExtra("Sort Order");
        switch (order) {
            case TITLE:
                return (m1, m2) -> m1.getMovieDetails().getOriginalTitle()
                        .compareTo(m2.getMovieDetails().getOriginalTitle());
            case LANGUAGE:
                return (m1, m2) -> m1.getMovieDetails().getOriginalLanguage()
                        .compareTo(m2.getMovieDetails().getOriginalLanguage());
            case STANDARD:
            default:
                return (m1, m2) -> 0;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (!fullScreenFragment) {
            movieListConstraint.setVisibility(View.VISIBLE);
        }
        fullScreenFragment = false;
    }
}
