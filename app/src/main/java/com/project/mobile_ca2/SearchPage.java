package com.project.mobile_ca2;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import com.project.mobile_ca2.adapters.HistoryAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.content.Intent;
import android.os.Bundle;

@SuppressWarnings("unused")
public class SearchPage extends AppCompatActivity {

    private final String TAG = SearchPage.class.getSimpleName();

    private SortOrder movieOrder = SortOrder.STANDARD;

    private FloatingActionButton sortByTitle;
    private FloatingActionButton sortByLanguage;

    public SearchView searchBar;
    private String username, searchHistory;
    RecyclerView recycler;
    private boolean open;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_page);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        recycler = findViewById(R.id.historyRecycler);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            username = bundle.getString("username");
        }

        SharedPreferences.Editor editor = sharedPref.edit();

        if (!sharedPref.contains(username)) {
            editor.putString(username, "").apply();
        }

        searchHistory = sharedPref.getString(username, "");
        String[] userHistory = {};
        if (searchHistory.contains(", ")) {
            userHistory = searchHistory.split(", ");
        }

        setAdapter(userHistory);

        searchBar = findViewById(R.id.searchBar);
        TextView txtUsername = findViewById(R.id.txt_username);
        txtUsername.bringToFront();


        ImageView logoutBtn = findViewById(R.id.logout_menu);
        ConstraintLayout logoutLayout = findViewById(R.id.logout_constraint);

        logoutBtn.setOnClickListener(view -> {
            if (logoutLayout.getVisibility() == View.GONE) {
                logoutLayout.setVisibility(View.VISIBLE);
            } else {
                logoutLayout.setVisibility(View.GONE);
            }
        });

        TextView txtLogout = findViewById(R.id.logout_txt);
        txtLogout.setOnClickListener(view -> {
            Intent intent = new Intent(this, SignUpPage.class);
            startActivity(intent);
        });

        txtUsername.setText("Welcome " + username);

        searchBar.setOnQueryTextFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                recycler.setVisibility(View.VISIBLE);
            } else {
                recycler.setVisibility(View.GONE);
            }
        });

        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String input) {

                Intent movieList = new Intent(getApplicationContext(), MovieList.class);

                Bundle bundle = new Bundle();

                if (input.length() == 4 && input.substring(0, 2).equals("20") || input.substring(0, 2).equals("19")
                        || input.substring(0, 2).equals("18")) {
                    int year = Integer.parseInt(input);
                    bundle.putInt("searchTermInt", year);
                } else {
                    bundle.putString("searchTermStr", input);
                    addToHistory(editor, input);
                }

                movieList.putExtras(bundle);
                movieList.putExtra("Sort Order", movieOrder);
                searchBar.setQuery("", false);

                startActivity(movieList);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String input) {
                return false;
            }
        });


        FloatingActionButton sortBtn = findViewById(R.id.sort_button);

        sortByTitle = findViewById(R.id.sort_by_title);
        sortByLanguage = findViewById(R.id.sort_by_language);

        sortBtn.setOnClickListener(view -> btnVisibility());

        sortByTitle.setOnClickListener(view -> movieOrder = SortOrder.TITLE);
        sortByLanguage.setOnClickListener(view -> movieOrder = SortOrder.LANGUAGE);
    }

    /**
     * Sets sort floating action buttons to either visible or gone
     */
    private void btnVisibility() {
        if (open) {
            sortByTitle.hide();
            sortByLanguage.hide();
            open = false;
        } else {
            sortByTitle.show();
            sortByLanguage.show();
            open = true;
        }
        sortByLanguage.setOnClickListener(view -> movieOrder = SortOrder.LANGUAGE);
    }

    /**
     * Adds search query to shared preference
     * @param editor
     * @param input
     */
    private void addToHistory(SharedPreferences.Editor editor, String input) {

        if (searchHistory.equals("")) {
            searchHistory += input;
        } else {

            String[] history = searchHistory.split(", ");

            for (String movie : history) {
                if (input.equals(movie)) {
                    return;
                }
            }
            searchHistory = searchHistory + ", " + input;
        }
        editor.remove(username).apply();
        editor.putString(username, searchHistory).apply();
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        searchHistory = sharedPref.getString(username, "");
        String[] userHistory;
        if (searchHistory.contains(", ")) {
            userHistory = searchHistory.split(", ");
        } else {
            userHistory = searchHistory.split(" ");
        }

        setAdapter(userHistory);
    }

    /**
     * Instantiates Adapter and passes user history into adapter
     * and sets layout of RecyclerView
     * @param historyList
     */
    private void setAdapter(String[] historyList) {
        HistoryAdapter historyAdapter = new HistoryAdapter(getBaseContext(), historyList);
        recycler.setAdapter(historyAdapter);
        recycler.setLayoutManager(new LinearLayoutManager(getApplication()));
    }

}
