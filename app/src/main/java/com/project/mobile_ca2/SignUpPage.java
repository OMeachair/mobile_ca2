package com.project.mobile_ca2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;


public class SignUpPage extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_page);

        Load.loadingMessage(new ProgressDialog(this), "Configuring App");
        final Button loginBtn = findViewById(R.id.login_button);
        loginBtn.setOnClickListener(view -> {
            Intent intent = new Intent(this, LoginAccount.class);
            startActivity(intent);
        });

        final Button signUpButton = findViewById(R.id.sign_up_button);
        signUpButton.setOnClickListener(view -> {
            Intent intent = new Intent(this, CreateAccount.class);
            startActivity(intent);
        });
    }

}
