package com.project.mobile_ca2;

/**
 * Used to get Sort Order of Movie objects
 */
public enum SortOrder {
    STANDARD,
    LANGUAGE,
    TITLE
}
