package com.project.mobile_ca2.adapters;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.support.annotation.NonNull;
import com.project.mobile_ca2.SearchPage;
import android.view.LayoutInflater;
import com.project.mobile_ca2.R;
import android.widget.TextView;
import android.content.Context;
import android.view.ViewGroup;
import android.view.View;

@SuppressWarnings("unused")
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private final String TAG = com.project.mobile_ca2.adapters.HistoryAdapter.class.getSimpleName();

    private String[] historyList;

    public HistoryAdapter(Context context, String[] historyList) {
        this.historyList = historyList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout historyItem_itemView;

        private TextView txtMovie_itemView;

        private ViewHolder(View itemView) {
            super(itemView);

            historyItem_itemView = itemView.findViewById(R.id.historyItem);

            txtMovie_itemView = itemView.findViewById(R.id.txtMovie);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.history_item, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        final String movie = historyList[position];

        final ConstraintLayout historyItem;

        TextView txtMovie;

        historyItem = viewHolder.historyItem_itemView;
        txtMovie = viewHolder.txtMovie_itemView;

        txtMovie.setText(movie);

        historyItem.setOnClickListener(v ->
                ((SearchPage)v.getContext()).searchBar.setQuery(movie,true)
        );

    }

    @Override
    public int getItemCount() {
        return historyList.length;
    }
}
