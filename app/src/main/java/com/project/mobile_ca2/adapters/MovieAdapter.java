package com.project.mobile_ca2.adapters;

import com.project.mobile_ca2.fragments.SingleMovieFragment;

import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;
import com.project.mobile_ca2.db.entities.Movie;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.content.SharedPreferences;
import com.project.mobile_ca2.MovieList;
import com.squareup.picasso.Picasso;
import android.view.LayoutInflater;
import android.os.CountDownTimer;
import com.project.mobile_ca2.R;

import android.widget.Button;
import android.widget.ImageView;
import android.content.Context;
import android.widget.TextView;
import android.view.ViewGroup;
import java.util.ArrayList;
import org.json.JSONObject;
import org.json.JSONArray;

import android.os.Bundle;
import android.view.View;
import android.util.Log;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private final String TAG = MovieAdapter.class.getSimpleName();

    private Context context;
    private MovieAdapter adapter = MovieAdapter.this;
    private List<Movie> movieList;

    public MovieAdapter(Context context, List<Movie> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout listItem_itemView;

        private ImageView imgPoster_itemView;

        private TextView txtID_itemView, txtTitle_itemView, txtOriginalLanguage_itemView, txtOriginalTitle_itemView, txtGenreIDs_itemView;

        public Button btnDelete_itemView;

        private ViewHolder(View itemView) {
            super(itemView);

            txtID_itemView = itemView.findViewById(R.id.txtID);

            listItem_itemView = itemView.findViewById(R.id.listItem);

            imgPoster_itemView = itemView.findViewById(R.id.imgPoster);

            txtTitle_itemView = itemView.findViewById(R.id.txtTitle);
            txtOriginalLanguage_itemView = itemView.findViewById(R.id.txtOriginalLanguage);
            txtOriginalTitle_itemView = itemView.findViewById(R.id.txtOriginalTitle);
            txtGenreIDs_itemView = itemView.findViewById(R.id.txtGenres);

            btnDelete_itemView = itemView.findViewById(R.id.btnDelete);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.list_item, parent, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        final Movie movie = movieList.get(position);
        Log.d(TAG, "movie: "+movie.toString());
        final ArrayList<String> values = getValues(movie);

        final ConstraintLayout listItem;

        ImageView imgPoster;

        TextView txtID;
        TextView txtTitle;
        TextView txtOriginalLanguage;
        TextView txtOriginalTitle;
        TextView txtGenreIDs;

        Button btnDelete;

        txtID = viewHolder.txtID_itemView;

        listItem = viewHolder.listItem_itemView;

        imgPoster = viewHolder.imgPoster_itemView;

        txtTitle = viewHolder.txtTitle_itemView;
        txtOriginalLanguage = viewHolder.txtOriginalLanguage_itemView;
        txtOriginalTitle = viewHolder.txtOriginalTitle_itemView;
        txtGenreIDs = viewHolder.txtGenreIDs_itemView;

        btnDelete = viewHolder.btnDelete_itemView;

        Log.d(TAG, "onBindViewHolder: "+values.get(10));

        if(!values.get(11).equals("")) {
            Picasso.get().load(values.get(11))
                    .fit()
                    .placeholder(R.drawable.poster_placeholder)
                    .error(R.drawable.poster_placeholder)
                    .into(imgPoster);
        } else {
            imgPoster.setImageDrawable(context.getResources().getDrawable(R.drawable.poster_placeholder));
        }

        txtID.setText(values.get(0));
        txtTitle.setText(values.get(5));
        txtOriginalLanguage.setText(values.get(8));
        txtOriginalTitle.setText(String.format("(%s)", values.get(9)));
        txtGenreIDs.setText(values.get(10));

        btnDelete.setOnClickListener(v -> {
            int index = 0;
            for (int i = 0; i < movieList.size(); ++i) {
                if(String.valueOf(movieList.get(i).getMovieDetails().getMovieID()).equals(txtID.getText().toString())){
                    index = i;
                    break;
                }
            }
            movieList.remove(index);
            adapter.notifyDataSetChanged();
        });

        listItem.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("values",values);

            listItem.setBackgroundColor(context.getResources().getColor(R.color.item_selected));
            new CountDownTimer(1000, 50) {

                @Override
                public void onTick(long arg0) {}

                @Override
                public void onFinish() {
                    listItem.setBackgroundColor(context.getResources().getColor(android.R.color.white));

                }
            }.start();

            ((MovieList) view.getContext()).movieListConstraint.setVisibility(View.INVISIBLE);
            ((MovieList) view.getContext()).singleMovieConstraint.setVisibility(View.VISIBLE);

            SingleMovieFragment singleMovie = new SingleMovieFragment();
            singleMovie.setArguments(bundle);

            FragmentManager fragmentManager = ((MovieList) view.getContext()).getSupportFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.singleMovieConstraint,singleMovie);
            fragmentTransaction.addToBackStack("movieStack").commit();

        });

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    private ArrayList<String> getValues(Movie movie) {

        ArrayList<String> values = new ArrayList<>();

        JSONObject movieObject, Movie, images, details, stats, genreIds;

        String keyValues = movie.toString();

        int movieID = 0;
        String voteCount = "";
        String id = "";
        String video = "";
        String voteAverage = "";
        String title = "";
        String popularity = "";
        String posterPath = "";
        String originalLanguage = "";
        String originalTitle = "";
        String ratings = "";
        String backdropPath = "";
        String adult = "";
        String overview = "";

        try {
            movieObject = new JSONObject(keyValues);
            Movie = movieObject.getJSONObject("Movie");
            images = Movie.getJSONObject("images");
            details = Movie.getJSONObject("MovieDetails");
            stats = Movie.getJSONObject("MovieStats");
            genreIds = Movie.getJSONObject("genreIDs");

            movieID = details.getInt("movieID");
            title = details.getString("title");
            originalTitle = details.getString("originalTitle");
            originalLanguage = details.getString("originalLanguage");
            overview = details.getString("overview");

            voteCount = stats.getString("voteCount");
            id = stats.getString("id");
            video = stats.getString("video");
            voteAverage = stats.getString("voteAverage");
            adult = stats.getString("adult");
            popularity = stats.getString("popularity");

            posterPath = images.getString("Poster");
            backdropPath = images.getString("Backdrop");

            ratings = genreIds.getString("Ratings");

        } catch (Exception e) {
            e.printStackTrace();
        }

        String genres = "";

        if(!ratings.equals("[]")) {

            genres = getGenres(ratings);
        }

        values.add(String.valueOf(movieID));
        values.add(voteCount);
        values.add(id);
        values.add(video);
        values.add(voteAverage);
        values.add(title);
        values.add(popularity);
        values.add(posterPath);
        values.add(originalLanguage);
        values.add(originalTitle);
        values.add(genres);
        values.add(backdropPath);
        values.add(adult);
        values.add(overview);

        return values;

    }

    private String getGenres(String genreIds){

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String genres = sharedPref.getString("genres","");

        genreIds = genreIds.replace("[","");
        genreIds = genreIds.replace("]","");

        String[] movieGenresIds = genreIds.split(",");

        StringBuilder genreStr = new StringBuilder();
        int count = 0;

        JSONObject genreObj;
        JSONArray genreList;

        try {
            genreObj = new JSONObject(genres);
            genreList = genreObj.getJSONArray("genres");

            for(int i = 0; i < genreList.length(); ++i){
                JSONObject idObj = genreList.getJSONObject(i);
                for (String movieGenresId : movieGenresIds) {
                    if (idObj.getInt("id") == Integer.parseInt(movieGenresId)) {
                        if (count == 0) {
                            genreStr.append(idObj.getString("name"));
                        } else {
                            genreStr.append(", ").append(idObj.getString("name"));
                        }
                        count++;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return genreStr.toString();
    }


}


