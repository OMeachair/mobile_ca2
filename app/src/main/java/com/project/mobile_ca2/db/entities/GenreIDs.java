package com.project.mobile_ca2.db.entities;

import org.json.JSONException;
import org.json.JSONArray;

public class GenreIDs {

    private int[] ids;

    public GenreIDs(JSONArray ids) {

        int[] parsedGenreIDs = new int[ids.length()];

        for (int j = 0;j < parsedGenreIDs.length;j++) {
            try {
                parsedGenreIDs[j] = ids.getInt(j);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.ids = parsedGenreIDs;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        int count = 0;

        sb.append("{\"Ratings\":[");
        for (int id : ids) {
            if(count == 0) {
                sb.append(id);
            } else {
                sb.append(", ").append(id);
            }
            count++;
        }

        sb.append("]}");

        return sb.toString();
    }
}
