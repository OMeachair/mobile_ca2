package com.project.mobile_ca2.db.entities;

public class Images {

    private String posterPath;
    private String backdropPath;

    public Images(String posterPath, String backdropPath) {
        final String BASE_URL = "http://image.tmdb.org/t/p/w500";

        final String IMAGE_UNAVAILABLE = "https://c8.alamy.com/comp/P2D5P1/photo-not-available-vector-icon-isolated-on-transparent-background-photo-not-available-logo-concept-P2D5P1.jpg";

        this.posterPath = posterPath.equals("") ? IMAGE_UNAVAILABLE : BASE_URL + posterPath;
        this.backdropPath = backdropPath.equals("") ? IMAGE_UNAVAILABLE : BASE_URL + backdropPath;

    }

    public String toString() {
        return "{" +
                    "\"Poster\":\"" + posterPath +"\", "+
                    "\"Backdrop\":\"" + backdropPath + "\""+
                "}";
    }
}
