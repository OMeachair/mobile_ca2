package com.project.mobile_ca2.db.entities;

public class Movie {
    private MovieStats movieStats;
    private MovieDetails movieDetails;
    private Images images;
    private GenreIDs genreIDs;

    public Movie(MovieStats movieStats, MovieDetails movieDetails, Images images, GenreIDs genreIDs) {
        this.movieStats = movieStats;
        this.images = images;
        this.movieDetails = movieDetails;
        this.genreIDs = genreIDs;
    }

    @Override
    public String toString() {
        return "{" +
                "   \"Movie\":{" +
                "\"images\":" + images.toString() + "," +
                movieDetails.toString() + "," +
                movieStats.toString() + ", " +
                "\"genreIDs\":" + genreIDs.toString() +
                "}}";
    }

     public MovieDetails getMovieDetails() {
        return movieDetails;
    }
}
