package com.project.mobile_ca2.db.entities;

public class MovieDetails {

    private int movieID;
    private String title;
    private String originalTitle;
    private String originalLanguage;
    private String overview;

    public MovieDetails(int movieID, String title, String originalTitle, String originalLanguage, String overview) {
        this.movieID = movieID;
        this.title = title;
        this.originalTitle = originalTitle;
        this.originalLanguage = originalLanguage;
        this.overview = overview;
    }

    @Override
    public String toString() {
        return "\"MovieDetails\":{" +
                  "\"movieID\":\"" + movieID + "\"," +
                  "\"title\":\""+ title + "\", " +
                  "\"originalTitle\":\"" + originalTitle + "\", " +
                  "\"originalLanguage\":\"" + originalLanguage + "\", " +
                  "\"overview\":\"" + overview + "\"" +
                "}";
    }

    public int getMovieID() { return movieID; }

    public void setMovieID(int movieID) { this.movieID = movieID; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
