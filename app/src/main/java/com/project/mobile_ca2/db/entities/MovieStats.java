package com.project.mobile_ca2.db.entities;

public class MovieStats {
    private int voteCount;
    private int id;
    private boolean video;
    private double voteAverage;
    private boolean adult;
    private double popularity;

    public MovieStats(int voteCount, int id, boolean video, double voteAverage, boolean adult, double popularity) {
        this.voteCount = voteCount;
        this.id = id;
        this.video = video;
        this.voteAverage = voteAverage;
        this.adult = adult;
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return "\"MovieStats\":{" +
                    "\"voteCount\":\"" + voteCount + "\", " +
                    "\"id\":\"" + id + "\", " +
                    "\"video\":\"" + video + "\", " +
                    "\"voteAverage\":\"" + voteAverage + "\", " +
                    "\"adult\":\"" + adult + "\", " +
                    "\"popularity\":\"" + popularity + "\"" +
                "}";
    }
}
