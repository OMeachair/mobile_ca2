package com.project.mobile_ca2.fragments;

import android.support.constraint.ConstraintLayout;
import android.support.annotation.NonNull;
import com.project.mobile_ca2.MovieList;
import android.content.ContentResolver;
import android.support.v4.app.Fragment;
import android.graphics.BitmapFactory;
import android.app.WallpaperManager;
import com.squareup.picasso.Picasso;
import android.view.LayoutInflater;
import android.provider.MediaStore;
import java.net.HttpURLConnection;
import android.widget.ImageView;
import com.project.mobile_ca2.R;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.os.AsyncTask;
import java.io.InputStream;
import android.util.Base64;
import android.os.Bundle;
import android.view.View;
import android.util.Log;
import java.net.URL;

public class FullscreenImageFragment extends Fragment{

    private final String TAG = FullscreenImageFragment.class.getSimpleName();

    public ConstraintLayout fullscreenImageConstraint;

    private ImageView imgFullscreen;

    public ImageView imgOptions;
    public TextView txtSaveImg, txtSetWallpaper;

    private Integer type;
    private String image, title, description;

    public FullscreenImageFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fullscreen_image, container, false);

        if (getArguments() != null) {

            type = getArguments().getInt("type");
            title = getArguments().getString("title");
            description = getArguments().getString("description");
            image = getArguments().getString("image");
        }

        if(getContext() != null) {
            ((MovieList) getContext()).fullScreenFragment = true;
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        getViews(view);

        if (type == 0) {

            Picasso.get()
                    .load(image)
                    .fit()
                    .placeholder(R.drawable.poster_placeholder)
                    .error(R.drawable.poster_placeholder)
                    .into(imgFullscreen);
        } else {
            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
            Bitmap decodedImage = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

            imgFullscreen.setImageBitmap(decodedImage);
        }



        txtSaveImg.setOnClickListener(view1 -> {
            AsyncTask asyncTask = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    try {

                        ContentResolver contentResolver =  getContext().getContentResolver();

                        HttpURLConnection connection = (HttpURLConnection) new URL(image).openConnection();
                        connection.connect();
                        InputStream input = connection.getInputStream();

                        Bitmap myBitmap = BitmapFactory.decodeStream(input);

                        MediaStore.Images.Media.insertImage(contentResolver, myBitmap, title , description);

                    } catch (Exception e){
                        Log.d(TAG, "onClick: "+e);
                    }

                    return "success";
                }
            };

            asyncTask.execute();
        });

        txtSetWallpaper.setOnClickListener(view12 -> {

            AsyncTask asyncTask = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    try {

                        WallpaperManager myWallpaperManager = WallpaperManager.getInstance(getContext());

                        HttpURLConnection connection = (HttpURLConnection) new URL(image).openConnection();
                        connection.connect();
                        InputStream input = connection.getInputStream();

                        myWallpaperManager.setStream(input);

                    } catch (Exception e){
                        Log.d(TAG, "onClick: "+e);
                    }

                    return "success";
                }
            };

            asyncTask.execute();


        });
    }

    /**
     * Instantiates UI views into memory
     * @param view
     */
    private void getViews(View view){

        fullscreenImageConstraint = view.findViewById(R.id.fullscreenImageConstraint);

        imgFullscreen = view.findViewById(R.id.imgFullscreen);
        imgOptions = view.findViewById(R.id.imgOptions);

        txtSaveImg = view.findViewById(R.id.txtSaveImg);
        txtSetWallpaper = view.findViewById(R.id.txtSetWallpaper);
    }


}

