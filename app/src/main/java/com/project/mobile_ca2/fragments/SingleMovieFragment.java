package com.project.mobile_ca2.fragments;

import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentTransaction;

import com.android.volley.toolbox.StringRequest;

import android.support.v4.app.FragmentManager;
import android.support.annotation.NonNull;

import com.android.volley.toolbox.Volley;
import com.project.mobile_ca2.MovieList;
import com.android.volley.RequestQueue;

import android.support.v4.app.Fragment;

import com.squareup.picasso.Picasso;

import android.view.LayoutInflater;

import com.android.volley.Request;
import com.project.mobile_ca2.R;

import android.widget.ImageView;
import android.widget.TextView;
import android.view.ViewGroup;

import org.json.JSONObject;

import java.util.ArrayList;

import android.view.View;
import android.os.Bundle;
import android.util.Log;

public class SingleMovieFragment extends Fragment {

    private final String TAG = SingleMovieFragment.class.getSimpleName();

    private String director, writer, lead, runtime, year, title, posterPath, originalLanguage, genres, overview;

    private TextView txtDirector, txtWriter, txtLead, txtRuntime, txtYear, txtTitle, txtOriginalLanguage, txtGenres, txtOverview;

    private ImageView imgPoster, imgOptions, imgX;

    private ConstraintLayout posterFullscreenConstraint;

    private FragmentTransaction fragmentTransaction;
    private FullscreenImageFragment fullscreenImageFragment = new FullscreenImageFragment();

    Bundle bundle = new Bundle();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bundle = getArguments();

            ArrayList<String> values = bundle.getStringArrayList("values");

            if (values != null) {
                title = values.get(5);
                originalLanguage = values.get(8);
                genres = values.get(10);
                posterPath = values.get(11);
                overview = values.get(13);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_movie, container, false);

        getViews(view);
        setViews();
        getIMDBInfo();

        imgPoster.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putInt("type", 0);
            bundle.putString("title", title + "_poster");
            bundle.putString("description", "Poster for the movie " + title);
            bundle.putString("image", posterPath);
            fullscreenImageFragment.setArguments(bundle);

            FragmentManager fragmentManager = ((MovieList) view.getContext()).getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.singleMovieConstraint, fullscreenImageFragment);
            fragmentTransaction.addToBackStack("movieStack").commit();

            posterFullscreenConstraint.setVisibility(View.VISIBLE);
        });

        imgX.setOnClickListener(view1 -> {
            FragmentManager fragmentManager = ((MovieList) view1.getContext()).getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fullscreenImageFragment);
            fragmentTransaction.commit();

            posterFullscreenConstraint.setVisibility(View.GONE);
        });

        imgOptions.setOnClickListener(view12 -> {

            if (fullscreenImageFragment.imgOptions.getVisibility() == View.VISIBLE) {
                fullscreenImageFragment.imgOptions.setVisibility(View.GONE);
                fullscreenImageFragment.txtSaveImg.setVisibility(View.GONE);
                fullscreenImageFragment.txtSetWallpaper.setVisibility(View.GONE);
            } else {
                fullscreenImageFragment.imgOptions.setVisibility(View.VISIBLE);
                fullscreenImageFragment.txtSaveImg.setVisibility(View.VISIBLE);
                fullscreenImageFragment.txtSetWallpaper.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }

    private void getIMDBInfo() {

        RequestQueue queue = null;

        if (getContext() != null) {
            queue = Volley.newRequestQueue(getContext());
        }

        String url = "http://www.omdbapi.com/?t=" + title + "&apikey=262b7119";

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    try {

                        JSONObject responseObj = new JSONObject(response);

                        String responseStatus = responseObj.getString("Response");

                        if (responseStatus.equalsIgnoreCase("true")) {

                            director = responseObj.getString("Director");
                            writer = responseObj.getString("Writer");
                            lead = responseObj.getString("Actors");
                            runtime = responseObj.getString("Runtime");
                            year = responseObj.getString("Year");

                            txtDirector.setText(director);
                            txtWriter.setText(writer);
                            txtLead.setText(lead);
                            txtRuntime.setText(runtime);
                            txtYear.setText(year);

                        } else {
                            Log.d(TAG, "onResponse: no info");
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "onResponse: " + e.toString());
                    }
                }, error -> System.out.println("GOT AN ERROR" + error));

        if (queue != null) {
            queue.add(stringRequest);
        }


    }

    private void getViews(View view) {

        posterFullscreenConstraint = view.findViewById(R.id.posterFullscreenConstraint);

        imgPoster = view.findViewById(R.id.imgPoster);
        imgOptions = view.findViewById(R.id.imgOptions);
        imgX = view.findViewById(R.id.imgX);

        txtDirector = view.findViewById(R.id.txtDirector);
        txtWriter = view.findViewById(R.id.txtWriter);
        txtLead = view.findViewById(R.id.txtLead);
        txtRuntime = view.findViewById(R.id.txtRuntime);
        txtYear = view.findViewById(R.id.txtYear);
        txtTitle = view.findViewById(R.id.txtTitle);
        txtOriginalLanguage = view.findViewById(R.id.txtLanguage);
        txtGenres = view.findViewById(R.id.txtGenre);
        txtOverview = view.findViewById(R.id.txtSynopsis);
    }

    private void setViews() {

        Picasso.get()
                .load(posterPath)
                .fit()
                .placeholder(R.drawable.poster_placeholder)
                .error(R.drawable.poster_placeholder)
                .into(imgPoster);

        txtDirector.setText(director);
        txtWriter.setText(writer);
        txtLead.setText(lead);
        txtRuntime.setText(runtime);
        txtTitle.setText(title);
        txtOriginalLanguage.setText(originalLanguage);
        txtGenres.setText(genres);
        txtOverview.setText(overview);

    }


}
