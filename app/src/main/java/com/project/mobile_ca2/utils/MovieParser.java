package com.project.mobile_ca2.utils;

import com.project.mobile_ca2.db.entities.MovieDetails;
import com.project.mobile_ca2.db.entities.MovieStats;
import com.project.mobile_ca2.db.entities.GenreIDs;
import com.project.mobile_ca2.db.entities.Images;
import com.project.mobile_ca2.db.entities.Movie;
import org.json.JSONObject;
import java.util.ArrayList;
import org.json.JSONArray;
import java.util.List;

@SuppressWarnings("unused")
public class MovieParser {

    private final String TAG = MovieParser.class.getSimpleName();

    private String mAPICall;

    List<Movie> movies = new ArrayList<>();

    public MovieParser(){}

    public MovieParser(int year) {
        String DISCOVER = "/discover";
        String BY_YEAR = "&year=";

        StringBuilder url = buildURL(DISCOVER, BY_YEAR);
        url.append(year);
        mAPICall = url.toString();
        System.out.println(mAPICall);
    }

    public MovieParser(String title) {
        String SEARCH = "/search";
        String BY_TITLE = "&query=";

        StringBuilder url = buildURL(SEARCH, BY_TITLE);
        url.append(title);

        mAPICall = url.toString();
    }

    /**
     * Builds API call depending either by year or title search
     * @param searchMethod
     * @param searchParameter
     * @return
     */
    private StringBuilder buildURL(String searchMethod, String searchParameter) {
        StringBuilder sb = new StringBuilder();

        String BASE_API_URL = "https://api.themoviedb.org/3";
        String MOVIE_SEARCH = "/movie?api_key=";
        String API_KEY = "cb33775e62681f48ef8192bf31c6a805";

        sb.append(BASE_API_URL);
        sb.append(searchMethod);
        sb.append(MOVIE_SEARCH);
        sb.append(API_KEY);
        sb.append(searchParameter);

        return sb;
    }

    /**
     * Parses JSON object into Movie object
     * @param id
     * @param jsonMovie
     * @return
     */
    public Movie parseMovie(int id, JSONObject jsonMovie) {

        MovieStats movieStats = getMovieStats(jsonMovie);
        Images images = getImages(jsonMovie);
        MovieDetails movieDetails = getMovieDetails(id, jsonMovie);
        JSONArray genreIDs = null;
        GenreIDs ids = null;

        try {
            genreIDs = jsonMovie.getJSONArray("genre_ids");

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(genreIDs != null) {
            ids = new GenreIDs(genreIDs);
        }

        return new Movie(movieStats, movieDetails, images, ids);

    }

    /**
     * Parses JSON paths for film posters into memory
     * @param obj
     * @return
     */
    private Images getImages(JSONObject obj) {

        String backdropPath = obj.optString("backdrop_path");
        String posterPath = obj.optString("poster_path");

        return new Images(backdropPath, posterPath);
    }

    /**
     * Parses JSON object film details into memory
     * @param id
     * @param obj
     * @return
     */
    private MovieDetails getMovieDetails(int id, JSONObject obj) {

        int movieID = 0;
        String title = "";
        String originalTitle = "";
        String originalLanguage = "";
        String overview = "";

        try {
            title = obj.getString("title");
            originalTitle = obj.getString("original_title");
            originalLanguage = obj.getString("original_language");
            overview = obj.getString("overview");

            if(title.contains("\"")){
                title = title.replace("\"","'");
            }
            if(originalTitle.contains("\"")){
                originalTitle = originalTitle.replace("\"","'");
            }
            if(overview.contains("\"")){
                overview = overview.replace("\"","'");
            }
            else if(overview.equals("")){
                 overview = "(Unavailable)";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return new MovieDetails(id, title, originalTitle, originalLanguage, overview);
    }

    /**
     * Parses movie stats from JSON film data into memory
     * @param obj
     * @return
     */
    private MovieStats getMovieStats(JSONObject obj) {

        int voteCount = 0;
        int id = 0;
        boolean video = false;
        double voteAverage = 0.0;
        boolean adult = false;
        double popularity = 0.0;

        try {
            voteCount = obj.getInt("vote_count");
            id = obj.getInt("id");
            video = obj.getBoolean("video");
            voteAverage = obj.getDouble("vote_average");
            adult = obj.getBoolean("adult");
            popularity = obj.getDouble("popularity");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new MovieStats(voteCount, id, video, voteAverage, adult, popularity);
    }


}
