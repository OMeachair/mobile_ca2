package com.project.mobile_ca2.utils;

import com.project.mobile_ca2.db.entities.Movie;

import java.util.List;

public class Movies {

    private List<Movie> movies;

    public Movies(List<Movie> movies) {
        this.movies = movies;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Movies: ");

        for (Movie m : movies) {
            sb.append("\t" + m);
            sb.append('\n');
        }

        return sb.toString();
    }
}
